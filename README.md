# Welcome to CCPD3V-T3@M #

Our operations team needs to create a basic project for the management of its users.

### Requirements? ###

* Angular (last version)
* User must be able to Login, LogOut
* Page CRUD for users (using api/token). 

### Service (backend)  ###

* API (ask for it)
* Are you fullstack? So, use your own backend service ;)

### Complete? ###

* Create a new branch with the following name: [FrontEnd-Version-YourName]
* Commit
* Push to your branch
* You can re-write .readme in your branch and explain how to set up your project.
* Enjoy :)